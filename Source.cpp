#include <vector>
#include <list>
#include <iostream>
#include "Graph.h"
using namespace std;

int main()
{
	int **arr, col, row, dis, vertice;	
	Graph g; //create graph with type integer (node means vertice)

	////Input the number of vertices
	//cout << "Input number of vertices : ";
	//cin >> vertice;
	vertice = 5;
	//create two dimension array (dynamic)
	arr = new int*[vertice];
	for (int i = 0; i < vertice; i++)
	{
		arr[i] = new int[vertice];
		for (int j = 0; j < vertice; j++)
		{
			arr[i][j] = 0;
		}
	}


	////Assign value if it's weight graph
	//cout << "\nAssign value if it's weight graph" << endl;
	//while (true)
	//{
	//	cout << "\n!!STOP CHANGE INPUT ALL 0" << endl;
	//	cout << "Row :";
	//	cin >> row;
	//	cout << "Column :";
	//	cin >> col;
	//	cout << "Weight : ";
	//	cin >> dis;
	//	if (dis == 0 && col == 0 && row == 0) //to stop
	//	{
	//		break;
	//	}
	//	else
	//	{
	//		arr[row - 1][col - 1] = dis; //Change value
	//	}
	//	cout << "Table now :\n";
	//	for (int i = 0; i < vertice; i++)
	//	{
	//		for (int j = 0; j < vertice; j++)
	//		{
	//			cout << arr[i][j] << " ";
	//		}
	//		cout << endl;
	//	}
	//}
	//

	arr[0][0] = 1;
	arr[0][1] = 2;
	arr[0][2] = 4;
	arr[0][3] = 3;
	arr[0][4] = 1;

	arr[1][0] = 2;
	arr[1][1] = 1;
	arr[1][2] = 3;
	arr[1][3] = 1;
	arr[1][4] = 1;

	arr[2][0] = 4;
	arr[2][1] = 3;
	arr[2][2] = 1;
	arr[2][3] = 1;
	arr[2][4] = 1;

	arr[3][0] = 3;
	arr[3][1] = 1;
	arr[3][2] = 1;
	arr[3][3] = 1;
	arr[3][4] = 4;

	arr[4][0] = 1;
	arr[4][1] = 1;
	arr[4][2] = 1;
	arr[4][3] = 4;
	arr[4][4] = 1;
	//print demo adjacency matrix 
	cout << "AdjencyMatrix :\n";
	for (int i = 0; i < vertice; i++)
	{

		for (int j = 0; j < vertice; j++)
		{
			cout << arr[i][j] << " ";
		}
		cout << endl;
	}
	//array to list
	g.GrapharrayTolist(arr, vertice);
	cout << endl;
	//Print graph
	g.printGraph();
	cout << endl;
	//Check graph
	if (g.isMultigraph() == false)
	{
		cout << "It isn't Multi graph\n";
	}

	if (g.isPseudograph() == true)
	{
		cout << "It is Pseudo graph\n";
	}
	else
	{
		cout << "It isn't Pseudo graph\n";
	}

	if (g.isDigraph() == true)
	{
		cout << "It is Directed graph\n";
	}
	else
	{
		cout << "It isn't Directed graph\n";
	}

	if (g.isWeightgraph() == true)
	{
		cout << "It is Weighted graph\n";
	}
	else
	{
		cout << "It isn't Weighted graph\n";
	}

	if (g.isCompletegraph() == true)
	{
		cout << "It is Complete graph\n";
	}
	else
	{
		cout << "It isn't Complete graph\n";
	}
	
	cout << endl;
	cout << "Shortest distance from A to all destination\n" << endl;
	//Find shortest path from A and print
	g.Shortestpath('A');
	
	cout << endl;
	cout << "Prim's Algorithm" << endl;
	//Prim algorithm
	g.Prim();
	
	cout << endl;
	//deallocate array
	for (int i = 0; i < vertice; ++i)
	{
		delete[] arr[i];
	}
	delete[] arr;
	cout << endl;
	system("PAUSE");
	return 0;
}
