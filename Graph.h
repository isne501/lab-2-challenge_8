#ifndef Graph_H
#define Graph_H
#include <list>
#include <iostream>
#include <climits>   
using namespace std;

class Node;

class Edge
{
public:


	Edge(Node* source, Node* dest, int weight) //constructor
	{
		Source = source;
		Destination = dest;
		Weight = weight;
	}



	Node* getSource() { return Source; } //return the pointer that point to source
	Node* getDestination() { return Destination; }//return the pointer that point to Destination
	int getWeight() { return Weight; }//return weight value
	void setWeight(int w) { Weight = w; }
private:
	
	Node* Source;
	Node* Destination;
	int Weight;
};

class Node
{
public:

	void addEdge(Node* dest, int weight) //add edge between vertice
	{
		Edge listedge(this, dest, weight);
		edge.push_back(listedge); //push edge to list 
	}

	void printEdge() //print edge
	{
		cout << "Vertice : " << getName() << endl;
		if (edge.empty()) //if vertice don't have edge to any Destination
		{
			cout << " No Destination" << endl;;
		}
		for (auto it = edge.begin(); it != edge.end(); it++) //if vertice have edge to any Destination loop for output every edge
		{
			Edge  pair = *it;
			cout << " To Destination " << pair.getDestination()->getName() << " with weight " << pair.getWeight() << endl;
		}
	}
	void setName(char Name) { name = Name; } //set name of vertice
	char getName() { return name; } //get the name of vertice
	list<Edge> getEdges() { return edge; } //return the list of edge each vertice
	int data;
private:
	char name;
	list<Edge> edge;
};

class Graph
{
public:

	void printGraph() //print graph
	{
		for (auto it = vertice.begin(); it != vertice.end(); it++)
		{
			Node vertice = *it;
			vertice.printEdge(); //print edge of each node
		}
	}

	void GrapharrayTolist(int **arr, int vertice_size) //change adjacency matrix to list
	{
		Node verticeName;

		for (int i = 0; i < vertice_size; i++)
		{
			verticeName.setName(65 + i); //assign name alphabet code A begin with 65 
			vertice.push_back(verticeName); //push the name of vertice
		}

		int i = 0; //for row of array
		for (auto it = vertice.begin(); it != vertice.end(); it++)
		{
			for (int j = 0; j < vertice_size; j++)
			{
				Node *dest = new Node; //create new node that will be Destination
				Node &source = *it;
				if (arr[i][j] == 0) //if weight equal to 0 continue the loop
				{
					continue;
				}
				else {
					dest->setName(65 + j); //assign name alphabet code A begin with 65 
					source.addEdge(dest, arr[i][j]); //addedge between source and Destination with weight equal to arr[i][j]
				}
			}
			i++; //change row
		}
	}

	bool isPseudograph()
	{
		//loop to check every vertice
		for (auto it = vertice.begin(); it != vertice.end(); it++)
		{
			Node vertice = *it;
			list <Edge> listedges = vertice.getEdges(); //get edge of vertice
			for (auto it2 = listedges.begin(); it2 != listedges.end(); it2++)//loop to check each edge of vertice
			{
				Node *source = it2->getSource(); //get source pointer of vertice
				Node *dest = it2->getDestination(); //get destnition pointer of vertice

				if (source->getName() == dest->getName() && it2->getWeight() != 0) ////if vertice has edge that connect with same source and destination with weight not equal to 0
				{
					return true;
				}
			}
		}
		return false;
	}

	bool isWeightgraph()
	{
		//loop to check every vertice
		for (auto it = vertice.begin(); it != vertice.end(); it++)
		{
			Node vertice = *it;
			list <Edge> listedges = vertice.getEdges(); //get edge of vertice
			for (auto it2 = listedges.begin(); it2 != listedges.end(); it2++) //loop to check each edge of vertice
			{
				if (it2->getWeight() != 0) //if iterator that point to list of edge if the weight not equal to zero that mean weight graph
				{
					return true;
				}
			}
		}
		return false;
	}

	bool isCompletegraph()
	{
		int i = 0;
		//loop to check every vertice
		for (auto it = vertice.begin(); it != vertice.end(); it++)
		{
			Node vertice = *it;
			list <Edge> listedges = vertice.getEdges(); //get edge of vertice
			for (auto it2 = listedges.begin(); it2 != listedges.end(); it2++)
			{
				Node *source = it2->getSource(); //get source pointer of vertice
				Node *dest = it2->getDestination(); //get destnition pointer of vertice
				i++;
				if (source->getName() == dest->getName())
				{
					i--; //minus the edge that psuedo
				}
			}
		}

		if (i == vertice.size()*(vertice.size() - 1))//formula to check complete graph
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	bool isDigraph()
	{
		//loop for fisrt vertice
		for (auto it = vertice.begin(); it != vertice.end(); it++)
		{
			Node vertice1 = *it;
			list <Edge> listedges1 = vertice1.getEdges(); //get edge of first vertice

			//loop for fisrt vertice each edge
			for (auto it2 = listedges1.begin(); it2 != listedges1.end(); it2++)
			{
				Node *source1 = it2->getSource(); //get source pointer of first vertice
				Node *dest1 = it2->getDestination(); //get destnition pointer of first vertice

				//loop for second vertice
				for (auto it3 = vertice.begin(); it3 != vertice.end(); it3++)
				{
					Node vertice2 = *it3;

					//check if it's same vertice or not (same vertice can't be compare)
					if (vertice2.getName() != vertice1.getName())
					{
						list <Edge> listedges2 = vertice2.getEdges(); //get edge of second vertice
						for (auto it4 = listedges2.begin(); it4 != listedges2.end(); it4++)
						{

							Node * source2 = it4->getSource(); //get source pointer of second vertice
							Node * dest2 = it4->getDestination();//get destnition pointer of second vertice

							//if vertice 1 cango to vertice2 and vertice 2 can go to vertice 1 that mean it isn't directed graph
							if (source1->getName() == dest2->getName() && source2->getName() == dest1->getName() && (source2->getName() != dest1->getName() || source2->getName() != dest2->getName()))
							{
								return false;
							}
						}
					}
				}
			}
		}
		if (isWeightgraph() == false) //if emtpy list 
		{
			return false;
		}
		return true;
	}

	bool isMultigraph() //adjacency matrix can't represented the mutilgraph
	{
		return false;
	}

	void Shortestpath(char source)
	{
		char name2 = source;
		vector<int> weightshort; //store min distance
		vector<char> destshort; //store name of min distance
		vector<int> weight; //store distance for dijkstra
		vector<char> dest; //store name of destination

		weightshort.push_back(0); //min distance itself  equal 0
		destshort.push_back(source); //push name

		weight.push_back(0);
		dest.push_back(source);

		//Get vertice and list of edge that have source eqaul to source(parameter input)
		for (auto it = vertice.begin(); it != vertice.end(); it++)
		{
			Node  vertex = *it;
			list<Edge> edges = vertex.getEdges();

			for (auto it2 = edges.begin(); it2 != edges.end(); it2++)
			{
				Node *Src = it2->getSource();
				Node *Desti = it2->getDestination();
				int dis = it2->getWeight();
				if (Src->getName() == source)
				{
					weight.push_back(dis);
					dest.push_back(Desti->getName());
				}
			}
		}
		int findMin = INT_MAX; //infinity (unreachable)
		char Minname = ' ';

		for (int i = 0; i < weight.size(); i++) //initial min distance and name
		{
			if (weight[i] > 0)
			{
				Minname = dest[i];
				findMin = weight[i];
				break;
			}
		}

		for (int i = 0; i < weight.size(); i++) //update new min distance and name
		{
			if (findMin > weight[i] && weight[i] > 0)
			{
				findMin = weight[i];
				Minname = dest[i];
			}
		}

		//push the shortest path
		destshort.push_back(Minname);
		weightshort.push_back(findMin);
		source = Minname; //move to new vertice that shortest distance  

		//if source and destination are same
		for (int i = 0; i < dest.size(); i++)
		{
			if (dest[i] == Minname)
			{
				weight[i] = 0; //distance equal 0
			}
		}

		while (true)
		{
			for (auto it3 = vertice.begin(); it3 != vertice.end(); it3++)
			{
				Node vertex = *it3;
				list<Edge> edges = vertex.getEdges();

				for (auto it4 = edges.begin(); it4 != edges.end(); it4++)
				{
					Node *Src = it4->getSource();
					Node *Desti = it4->getDestination();
					int dis = it4->getWeight();
					bool status = false;

					if (Src->getName() == source)
					{

						for (int i = 0; i < destshort.size(); i++)
						{
							if (destshort[i] == Desti->getName()) //if already shortest
							{
								status = true;
								break;
							}
						}

						if (status == false)
						{
							for (int i = 0; i < dest.size(); i++)
							{
								if (dest[i] == Desti->getName())
								{
									if (weight[i] > findMin + dis) //if old path to destination more than new path to destination update weight and break loop
									{
										weight[i] = findMin + dis;
										status = true;
										break;
									}
								}
							}
							if (status == false)
							{
								weight.push_back(dis + findMin); //the shortest value from soure to destination 
								dest.push_back(Desti->getName());
							}
						}
					}
				}
			}

			for (int i = 0; i < weight.size(); i++)
			{
				if (weight[i] > 0)
				{
					Minname = dest[i];
					findMin = weight[i];
					break;
				}
			}

			for (int i = 0; i < weight.size(); i++)
			{
				if (findMin > weight[i] && weight[i] > 0)
				{
					Minname = dest[i];
					findMin = weight[i];
				}
			}

			if (Minname != destshort.back())
			{
				destshort.push_back(Minname);
				weightshort.push_back(findMin);
			}
			for (int i = 0; i < dest.size(); i++)
			{
				if (dest[i] == Minname)
				{
					weight[i] = 0;
				}
			}
			int count = 0;
			source = Minname;
			for (int i = 0; i < weight.size(); i++)
			{
				if (weight[i] == 0)
				{
					count++;
				}
			}
			if (count == weight.size())
			{
				break;
			}
		}

		//print shortest distance
		int i = 0;
		int loop = 0;
		while (i != destshort.size())
		{
			if (name2 == destshort[i] && weightshort[i] > 0)
			{
				cout << "";
			}
			else if (weightshort[i] == INT_MAX)
			{
				cout << "";
			}
			else
				cout << name2 << " To " << destshort[i] << " with distance " << weightshort[i] << endl;
			i++;
			loop++;
		}
		if (destshort.size() - 1 != vertice.size())
		{
			cout << "Remain destination is Unreachable";
		}
	}

	void Prim()
	{
		//create array 
		int *Distance = new int[vertice.size()];
		int *neighbor= new int[vertice.size()];
		bool *visit = new bool[vertice.size()];
		int tmp = 0;
		
		int FirstArr = 0; //initial value
		for (int i = 0; i < vertice.size(); i++)
		{
			Distance[i] = INT_MAX;
			visit[i] = false;
		}
		Distance[0] = 0;
		neighbor[0] = -1;
		//loop to check every vertice
		for (int j = 0; j < vertice.size() -1; j++)
		{
			int u = minDistance(Distance, visit); //get index that have min distance
			visit[u] = true; //visit already
			FirstArr = 0;
			for (auto it = vertice.begin(); FirstArr <= u; it++)
			{
				if (FirstArr == u) //if same index
				{
					Node &node = *it;
					list<Edge> edge = node.getEdges(); //get edge
					for (int v = 0; v < vertice.size(); v++)
					{
						int SecondArr = 0;
						for (auto it2 = edge.begin(); SecondArr <= v; it2++)
						{
							Edge &tmpE = *it2;
							tmp = tmpE.getWeight(); //get weight
							SecondArr++;
							if (tmp && visit[v] == false && tmp < Distance[v])
							{
								neighbor[v] = u;
								Distance[v] = tmp;
							}
}
						}
					}
					FirstArr++;
				}
		}	
		printPrim(neighbor); //print prim
		//deallocate
		delete[] Distance;
		delete[] neighbor;
		delete[] visit;
	}
private:
	//printPrim
	void printPrim(int *neighbor)
	{
		cout << "Edge\tWeight" << endl;
		for (int i = 1; i < vertice.size(); i++)
		{
			char Source = 65 + neighbor[i]; //Source A
			char Dest = 65 + i; //Destination
			cout << Source << "-" << Dest;
			int tmp = 0;
			int FirstArr = 0;
			for (auto it = vertice.begin(); FirstArr <= i; it++)
			{
				if (FirstArr == i)
				{
					Node &node = *it;
					list<Edge> edge = node.getEdges(); //get edge
					int SecondArr = 0;
					for (auto it2 = edge.begin(); SecondArr <= neighbor[i]; it2++)
					{
						if (SecondArr == neighbor[i])
						{
							Edge &tmpE = *it2;
							tmp = tmpE.getWeight();
							cout << "       " << tmp << endl; //Print weight of edge
						}
						SecondArr++;
					}
				}
				FirstArr++;
			}
		}
	}
	//find the index of mindistance
	int minDistance(int *Distance, bool visit[])
	{
		int min = INT_MAX, min_index;
		for (int v = 0; v < vertice.size(); v++)
		{
			if (visit[v] == false && Distance[v] <= min)
			{
				min = Distance[v];
				min_index = v;
			}
		}
		return min_index;
	}

	list<Node> vertice;
};
#endif
